package com.alloy.demopdfitext.util

import java.io.ByteArrayOutputStream
import com.itextpdf.text.pdf.PdfWriter
import com.itextpdf.text.pdf.PdfPCell
import com.alloy.demopdfitext.model.City
import com.itextpdf.text.*
import com.itextpdf.text.pdf.PdfPTable
import java.io.ByteArrayInputStream
import java.util.logging.Level
import java.util.logging.Logger
import com.itextpdf.text.Paragraph
import org.springframework.util.ResourceUtils
import com.itextpdf.text.pdf.BaseFont




class GeneratePdfReport{

    companion object {

        private val catFont = Font(Font.FontFamily.TIMES_ROMAN, 18F,
                Font.BOLD)


        // FONT USED TO READ FUCKING CYRILLIC CHARACTERS
        const val fontpath:String = "static/fonts/FreeSans.ttf"
        val font = FontFactory.getFont(fontpath, BaseFont.IDENTITY_H, true)
        fun citiesReport(cities:Collection<City>):ByteArrayInputStream{

            val document = Document()
            val out = ByteArrayOutputStream()

            try {


                val preface = Paragraph()
                // We add one empty line
                addEmptyLine(preface, 1)
                // Lets write a big header
                preface.add(Paragraph("Title of the document", catFont))
                preface.alignment = Element.ALIGN_CENTER


                val table = PdfPTable(3)
                table.widthPercentage = 100f
                table.setWidths(intArrayOf(1, 3, 3))


                val headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD)

                var hcell: PdfPCell
                hcell = PdfPCell(Phrase("Id", headFont))
                hcell.horizontalAlignment = Element.ALIGN_CENTER
                table.addCell(hcell)

                hcell = PdfPCell(Phrase("Name", headFont))
                hcell.horizontalAlignment = Element.ALIGN_CENTER
                table.addCell(hcell)

                hcell = PdfPCell(Phrase("Population", headFont))
                hcell.horizontalAlignment = Element.ALIGN_CENTER
                table.addCell(hcell)

                for (city in cities) {

                    var cell = PdfPCell(Phrase(city.id!!.toString()))

                    cell.verticalAlignment = Element.ALIGN_MIDDLE
                    cell.horizontalAlignment = Element.ALIGN_CENTER
                    table.addCell(cell)

                    cell = PdfPCell(Phrase(city.name))
                    cell.paddingLeft = 5f
                    cell.verticalAlignment = Element.ALIGN_MIDDLE
                    cell.horizontalAlignment = Element.ALIGN_LEFT
                    table.addCell(cell)

                    cell = PdfPCell(Phrase(city.population.toString()))
                    cell.verticalAlignment = Element.ALIGN_MIDDLE
                    cell.horizontalAlignment = Element.ALIGN_RIGHT
                    cell.paddingRight = 5f
                    table.addCell(cell)
                }


                val rightText = Paragraph("This is right aligned text")
                rightText.alignment = Element.ALIGN_RIGHT
                val centerText =  Paragraph("This is centered text")
                centerText.alignment = Element.ALIGN_CENTER
                addEmptyLine(rightText,3)
                addEmptyLine(centerText,3)









                PdfWriter.getInstance(document, out)
                document.open()
                document.add(getLogo(110F, 110F))
                document.add(preface)
                document.add(rightText)
                document.add(centerText)

                document.add(table)

                document.add(getTextAtTheBottomRightCorner("Hello", 3))




                document.close()

            }catch (ex:DocumentException){
                Logger.getLogger(GeneratePdfReport::class.java.name).log(Level.SEVERE, null, ex)

            }


            return  ByteArrayInputStream(out.toByteArray())


        }


        fun reportPdfDemo():ByteArrayInputStream{
            val document = Document()
            val out = ByteArrayOutputStream()


            try {


                PdfWriter.getInstance(document, out)
                document.open()
                document.add(getLogo(80F, 80F))
                document.add(Paragraph(" "))
                document.add(getRightAlignedText("Директору РВШБУ",0, 12F))
                document.add(getRightAlignedText("Абдураупову Р.Р.",0,12F))
                document.add(getRightAlignedText("от Начальника отдела",0,12F))
                document.add(getRightAlignedText("Информационно-коммуникационных",0,12F))
                document.add(getRightAlignedText("технологий и дистанционного обучения",0,12F))
                document.add(getRightAlignedText("Тощмухаммедова Р.О.",3,12F))
                document.add(getTitleCenter("РАПОРТ",4, 18F))

                document.add(getSimpleTextParagraph(
                        "Lorem Ipsum is simply dummy text of the printing and " +
                                "typesetting industry. Lorem Ipsum has been the industry's " +
                                "standard dummy text ever since the 1500s, " +
                                "when an unknown printer took a galley of type and scrambled it to make a type specimen book. " +
                                "It has survived not only five centuries, but also the leap into electronic typesetting, remaining " +
                                "essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing " +
                                "Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including" +
                                " versions of Lorem Ipsum", spacing = 6))


                document.add(getTextAtTheBottomRightCorner("Тощмухаммедов Р.О.", 1))
                document.add(getTextAtTheBottomRightCorner("Подпись   __________", 0))



                document.close()




            }catch (ex:DocumentException){
                Logger.getLogger(GeneratePdfReport::class.java.name).log(Level.SEVERE, null, ex)

            }





            return  ByteArrayInputStream(out.toByteArray())
        }


        private fun addEmptyLine(paragraph: Paragraph, number: Int){
            for (i in 0 until number) {
                paragraph.add(Paragraph(" "))
            }
        }

        private fun getLogo(newWidth:Float, newHeight:Float):Image{

            val img = Image.getInstance(ResourceUtils.getFile("classpath:static/logo.png").absolutePath)
            img.scaleAbsolute(newWidth, newHeight)
            img.alignment = Element.ALIGN_CENTER

            return img

        }


        private fun getSimpleTextParagraph(yourText:String, fontSize:Float = 12F, spacing: Int = 0):Paragraph{
            font.size = fontSize
            val simpleText = Paragraph(yourText, font)
            addEmptyLine(simpleText, spacing)
            return simpleText
        }

        private fun getTitleCenter(yourTitle:String, spacing: Int, fontSize:Float = 12F):Paragraph{
            font.size = fontSize
            val centerText =  Paragraph(yourTitle, font)
            centerText.alignment = Element.ALIGN_CENTER
            addEmptyLine(centerText,spacing)
            return centerText
        }

        private fun getRightAlignedText(yourText:String, spacing:Int, fontSize:Float = 12F):Paragraph{
            font.size = fontSize
            val rightText = Paragraph(yourText, font)
            rightText.alignment = Element.ALIGN_RIGHT
            addEmptyLine(rightText,spacing)
            return rightText
        }


        private fun getFreshTableWithHeaders(numberOfHeaders:Int, listOfHeaders: MutableList<String>, listOfItems:Collection<String>):PdfPTable{


            val table = PdfPTable(numberOfHeaders)


            listOfHeaders.forEach { columntTitle ->
                run {
                    val header = PdfPCell()
                    header.backgroundColor = BaseColor.LIGHT_GRAY
                    header.borderWidth = 2F
                    header.phrase = Phrase(columntTitle)
                    table.addCell(header)
                }
            }





            return table

        }

        private fun insertDemoDataIntoTable(table:PdfPTable){
            table.addCell("Hello Cell1")
            table.addCell("Hello Cell2")
            table.addCell("Hello Cell3")
        }

        private fun getTextAtTheBottomRightCorner(yourText: String,spacing:Int, fontSize:Float = 12F):Paragraph{
            font.size = fontSize
            val rightBottomCorner =  Paragraph(yourText, font)
            rightBottomCorner.alignment = Element.ALIGN_BOTTOM
            rightBottomCorner.alignment = Element.ALIGN_RIGHT
            addEmptyLine(rightBottomCorner, spacing)
            return rightBottomCorner
        }



    }

}