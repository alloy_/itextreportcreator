package com.alloy.demopdfitext

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DemopdfitextApplication

fun main(args: Array<String>) {
	runApplication<DemopdfitextApplication>(*args)
}
