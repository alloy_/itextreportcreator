package com.alloy.demopdfitext.model

import javax.persistence.*


@Entity
@Table(name = "CITIES")
class City{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    var name:String? = null
    var population:Int? = null

}