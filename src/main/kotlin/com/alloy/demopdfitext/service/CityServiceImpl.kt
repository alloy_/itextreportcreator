package com.alloy.demopdfitext.service

import com.alloy.demopdfitext.model.City
import com.alloy.demopdfitext.repository.CityRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CityServiceImpl @Autowired constructor(

        val cityRepository: CityRepository
) :ICityService{
    override fun findAll(): Collection<City> {
        return cityRepository.findAll()
    }

}