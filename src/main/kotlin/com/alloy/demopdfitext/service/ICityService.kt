package com.alloy.demopdfitext.service

import com.alloy.demopdfitext.model.City

interface ICityService{
    fun findAll():Collection<City>
}