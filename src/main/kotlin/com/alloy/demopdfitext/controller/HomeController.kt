package com.alloy.demopdfitext.controller

import com.alloy.demopdfitext.service.ICityService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.InputStreamResource
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import com.alloy.demopdfitext.util.GeneratePdfReport
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType


@Controller
@RequestMapping("/main")
class HomeController@Autowired constructor(
        val  cityServiceImpl: ICityService
){

    @GetMapping("/")
    fun getIndex():String{
        return "index"
    }


    @GetMapping("/pdfdemo")
    fun getDemo(): ResponseEntity<InputStreamResource> {
        val cities = cityServiceImpl.findAll()

        val bis = GeneratePdfReport.citiesReport(cities)
        val headers = HttpHeaders()
        headers.add("Content-Disposition", "inline; filename=citiesreport.pdf")


        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(InputStreamResource(bis))
    }







    @GetMapping("/pdfreport")
    fun getpdfreport():ResponseEntity<InputStreamResource>{



        val bis = GeneratePdfReport.reportPdfDemo()
        val headers = HttpHeaders()
        headers.add("Content-Disposition", "inline; filename=citiesreport.pdf")


        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(InputStreamResource(bis))




    }




}