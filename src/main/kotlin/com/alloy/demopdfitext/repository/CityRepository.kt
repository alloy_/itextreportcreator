package com.alloy.demopdfitext.repository

import com.alloy.demopdfitext.model.City
import org.springframework.data.jpa.repository.JpaRepository

interface CityRepository : JpaRepository<City, Long>